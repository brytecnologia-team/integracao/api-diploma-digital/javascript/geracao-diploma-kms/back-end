# API de Geração de assinatura de Diploma Digital com certificado em nuvem

Este é exemplo backend de integração dos serviços da API de assinatura com clientes baseados em tecnologia Node.js, que utilizam o KMS.

Este exemplo apresenta os passos necessários para a geração de assinatura de diploma digital utilizando-se do certificado em nuvem no KMS ou no HSM Dinamo.

* *BRy KMS*: Certificado está armazenado na Plataforma de Certificados em Nuvem da BRy Tecnologia. Para este modo de acesso ao certificado o Tipo da Credencial selecionado deve ser **BRYKMS**. Para credenciais do KMS com tipo **PIN** o Valor da Credencial corresponde a senha de acesso ao certificado em nuvem, enquanto que para credenciais do tipo **TOKEN** o Valor da Credencial corresponde a um token de autorização do acesso ao certificado. Mais informações sobre o serviço de certificados em nuvem podem ser encontradas em nossa documentação da [API de Certificado em Nuvem](https://api-assinatura.bry.com.br/api-certificado-em-nuvem) e em nosso [Repositório Swagger da API de Assinatura](https://hub2.bry.com.br/swagger-ui/index.html#/) no schema kms_data.

* *HSM Dinamo*: Certificado está armazenado em um HSM Dinamo. Para este modo de acesso ao certificado o Tipo da Credencial selecionado deve ser **DINAMO**, Tipo da credencial do Dinamo (OTP, Token, Usuario e senha) e o Valor da Credencial do Dinamo corresponde ao par usuário/senha, token de acesso, PIN da conta, ou OTP do usuário. É possível verificar um exemplo de como usar as credenciais do DINAMO em nosso [Repositório Swagger da API de Assinatura](https://hub2.bry.com.br/swagger-ui/index.html#/) no schema kms_data.

### Tech

O exemplo utiliza das bibliotecas Node abaixo:
* [Node] - Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine.
* [Express] - Fast, unopinionated, minimalist web framework for Node.js
* [Axios] - Promise based HTTP client for the browser and node.js
* [Multer] - Node.js middleware for handling `multipart/form-data`.

**Observação**

Esta API em Node pode ser executada juntamente com o [frontend](https://gitlab.com/brytecnologia-team/integracao/api-diploma-digital/react/geracao-diploma-kms) de exemplo feito em React

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a BRy Extension no seu browser, assim como ter um certificado importado.

**Observação**

É necessário ter o [Node] instalado na sua máquina para a instalação das dependências e execução do projeto, além de executar esta aplicação juntamente com o backend.

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| JWT_TOKEN | Access Token para o consumo do serviço (JWT). | CredencialConfig
| KMS_CREDENCIAL | PIN cadastrado no KMS, codificado em Base64 | KMSConfig

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. Utilizamos o Node versão 12 e yarn (ou npm) para a instalação das dependências.

Comandos:

Instalar dependências:

    -yarn
    
ou
    
    -npm install

Executar programa:

    -yarn dev
    
ou
    
    -npm run dev

  [Node]: <https://nodejs.org/en/>
  [Express]: <https://expressjs.com/>
  [Axios]: <https://github.com/axios/axios>
  [Multer]: <https://github.com/expressjs/multer>
